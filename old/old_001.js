/*----------------------------------------------
    
    searchbar with custom search suggestions
    - written by HT (@glenthemes)
	
    github.com/glenthemes/search-dropdown
    
----------------------------------------------*/

document.addEventListener("DOMContentLoaded",() => {

/*---------------------*/

let llvlq = Date.now();
let nvtwq = 1500;

let srchSpeed = parseInt(getComputedStyle(document.documentElement).getPropertyValue("--Search-Suggestions-Expand-Speed").trim());

/*---------------------*/

// <input>
let srchField = document.querySelector(".searchbar-field");

// <button>
let srchBtn = document.querySelector(".searchbar-button button");

// suggestions box wrapper
let srchSgstnsWrap = document.querySelector(".search-suggestions-wrapper");

// suggestions box
let srchSgstns = document.querySelector(".search-suggestions");

// the suggestions
let srchItems = document.querySelectorAll(".search-suggestions a");

srchField.setAttribute("autocomplete","off");

/*----- SEARCH SUGGESTIONS HEIGHT -----*/
function xsmoc(){
	let lpyxg = setInterval(() => {
		if(Date.now() - llvlq > nvtwq){
			clearInterval(lpyxg);
		} else {
			if(srchSgstns.offsetHeight > 0){
				clearInterval(lpyxg);
				srchSgstns.style.setProperty("--Search-Suggestions-Height",srchSgstns.offsetHeight + "px");
				// console.log(srchSgstns.offsetHeight)
			}
		}
	},0);
}

xsmoc();

window.addEventListener("load", () => {
	xsmoc()
})

/*----- KEEP TRACK OF TYPING IN <INPUT> -----*/
let srchFieldVal = "";
srchField.addEventListener("keyup", (e) => {
	if(e.keyCode !== 38 || e.keyCode !==  40){
		if(srchField.value.trim() !== ""){
			srchFieldVal = srchField.value;
		}		
	}	
})

// temporarily change sidebar text to the suggestion that's currently hovered
srchItems.forEach(senre => {
	senre.addEventListener("mouseover", () => {
		let senreTxt = senre.textContent;
		srchField.value = senreTxt;
		
		senre.addEventListener("mouseleave", () => {
			srchField.value = srchFieldVal;
		});
	})
	
	// on mouse move, remove any .hov
	// and re-enable css hover behavior
	senre.addEventListener("mousemove", () => {
		let kpont = document.querySelector(".search-suggestions a.hov");
		if(kpont !== null){
			kpont.classList.remove("hov")
		}
		srchSgstns.classList.remove("disb-hov")
	});
})

// open the box [1/2]
srchField.addEventListener("click", () => {
	openSrchBox()
});

srchField.addEventListener("focus", () => {
	openSrchBox()
});

// open the box [2/2]
function openSrchBox(){
	if(!srchField.matches(".is-open")){
		srchSgstnsWrap.style.display = "block";
		srchSgstnsWrap.style.visibility = "visible";
		
		srchSgstns.style.height = 0;
		setTimeout(() => {
			srchSgstns.classList.add("expand")
		},1);
		
		setTimeout(() => {
			srchField.classList.add("is-open");
		},srchSpeed)
	}
}

// close the box [1/2]
document.addEventListener("click", (e) => {
	if(!e.target.closest(".search-suggestions, .searchbar-button button")){
		closeSrchBox.call(e.target, e);
	}
});

document.addEventListener("keydown", (e) => {
	if(e.keyCode == 27){
		closeSrchBox()
	}
})

// close the box [2/2]
function closeSrchBox(){
	if(srchField.matches(".is-open")){
		srchItems.forEach(waa => {
			if(waa.matches(".hov")){
				waa.classList.remove("hov")
			}
		})
		
		srchSgstns.classList.remove("expand");

		setTimeout(() => {
			srchSgstns.style.height = 0;
			srchSgstnsWrap.style.display = "";
			srchSgstnsWrap.style.visibility = "";
			srchField.classList.remove("is-open");
			
			srchSgstns.classList.remove("z-entered");
			srchSgstns.classList.remove("disb-hov")
		},srchSpeed)
		
		
	}
}

// arrow keys to navigate the suggestions list
let hov = new Event("mouseover");

document.addEventListener("keydown", (e) => {
	/*------- DOWN ARROW KEY -------*/
	if(e.keyCode == 40){
		
		// if <input> is focused
		if(srchField == document.activeElement){
			// if sug box is closed, open it
			if(!srchField.matches(".is-open")){
				openSrchBox()
			}
			
			// if sug box is already open
			else {
				srchSgstns.classList.add("disb-hov");
				
				srchItems.forEach((lah, i) => {
					// if no items have been hovered
					if(document.querySelector(".search-suggestions a.hov") == null){
						if(!srchSgstns.matches(".z-entered")){

							// add focus to the first item
							if(i == 0){
								lah.dispatchEvent(hov);
								lah.classList.add("hov");

								setTimeout(() => {
									srchSgstns.classList.add("z-entered");
								},0)

							}//end: if first suggestion
						}//end: if items haven't been hov'ed at all

					}//end: if .hov doesn't exist
				})//end: suggestion(s) forEach
				
				if(srchSgstns.matches(".z-entered")){
					let currentItem = document.querySelector(".search-suggestions a.hov");
					let nextItem = currentItem.nextElementSibling;

					if(currentItem !== null){
						if(nextItem !== null){
							currentItem.classList.remove("hov");

							nextItem.classList.add("hov");
							nextItem.dispatchEvent(hov);
						}
						
						// when the bottom is reached,
						// go back to the top and start again
						else {
							srchItems.forEach((lah, i) => {
								if(i == 0){
									currentItem.classList.remove("hov");
									lah.classList.add("hov");
									lah.dispatchEvent(hov);
								}
							});
						}
					}
				}
			}//end: if sug box is already open
		}//end: if <input> is focused (doc.activeElement)
	}//end if down-arrow
	
	/*------- UP ARROW KEY -------*/
	else if(e.keyCode == 38){
		// if <input> is focused
		if(srchField == document.activeElement){
			// if sug box is closed, open it
			if(!srchField.matches(".is-open")){
				openSrchBox()
			}
			
			// if sug box is already open
			else {
				srchSgstns.classList.add("disb-hov")

				srchItems.forEach((lah, i) => {
					// if no items have been hovered
					if(document.querySelector(".search-suggestions a.hov") == null){
						if(!srchSgstns.matches(".z-entered")){

							// add focus to the last item
							if(i == srchItems.length-1){
								lah.dispatchEvent(hov);
								lah.classList.add("hov");

								setTimeout(() => {
									srchSgstns.classList.add("z-entered");
								},0)

							}//end: if last suggestion
						}//end: if items haven't been hov'ed at all

					}//end: if .hov doesn't exist
				})//end: suggestion(s) forEach

				if(srchSgstns.matches(".z-entered")){
					let currentItem = document.querySelector(".search-suggestions a.hov");
					let prevItem = currentItem.previousElementSibling;

					if(currentItem !== null){
						if(prevItem !== null){
							currentItem.classList.remove("hov");

							prevItem.classList.add("hov");
							prevItem.dispatchEvent(hov);
						}
						
						// when the top is reached,
						// go back to the bottom and start again
						else {
							srchItems.forEach((lah, i) => {
								if(i == srchItems.length-1){
									currentItem.classList.remove("hov");
									lah.classList.add("hov");
									lah.dispatchEvent(hov);
								}
							});
						}
					}// if .hov exists
				}// if sug box is open (.z-entered)
			}//end; if sug box is open (.is-open)
		}//end: if <input> is focused (doc.activeElement)
		
		
	}//end if up-arrow
})//end keydown event

});//end DOMcontentloaded
